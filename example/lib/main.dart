import 'easyping.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Easy Ping Example',
      home: MyHomePage(title: 'Easy Ping Example'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double lastPing = 0;
  bool pinging = false;
  String address = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              pinging?Text('Pinging $address...'):Text('$lastPing ms to "$address"'),
              pinging?LinearProgressIndicator():Container(),
              Divider(),
              pinging?Container():TextField(
                onChanged: (t){
                  address = t;
                },
              ),
              pinging?Container():MaterialButton(
                color: Colors.lightBlue,
                onPressed: () async{
                  setState(() {pinging = true;});
                  lastPing = await ping(address);
                  setState(() {
                    pinging = false;
                  });
                },
                child: Text('Ping Address...'),
              )
            ],
          ),
        ),
      ),
    );
  }
}